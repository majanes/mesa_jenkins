#!/usr/bin/env python3

import glob
import os
import subprocess
import sys
try:
    from urllib2 import urlopen, urlencode, URLError, HTTPError, quote
except:
    from urllib.request import urlopen, URLError, HTTPError, quote
    from urllib.parse import urlencode
import ast
import time

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from utils.utils import reliable_url_open

def is_excluded(a_host):
    """don't send a build to the master system, which does not clean workspaces"""
    if a_host == "master":
        return True
    if a_host == "Built-In Node":
        return True
    return False

def remove_workspace_files():
    """On the current system, delete all repos in conformance workspaces"""
    try:
        os.chdir(f"{os.path.dirname(os.path.abspath(sys.argv[0]))}/../docker")
        subprocess.run(["docker",
                        "image",
                        "prune",
                        "-f"
                        ], check=True)
        subprocess.run(["docker-compose",
                        "down"
                    ], check=True)
        subprocess.run(["docker",
                        "volume",
                        "prune",
                        "-f"
                        ], check=True)
    except Exception as error:
        print(f"Warning: could not remove volumes due to error: {error}")
    for a_workspace in glob.glob("/home/jenkins/workspace/*conformance*"):
        try:
            os.chdir(f"{a_workspace}/docker")
            subprocess.run(["docker-compose",
                            "run",
                            "-u", "root:root",
                            "conformance_m64",
                            "rm", "-rf", "/sources"
                            ], check=True)

        except Exception as error:
            print(f"Warning: could not remove {a_workspace} due to error: {error}")

def trigger_builds():
    """On any system, determine which test machines are online and
    send a build to them"""
    server = "mesa-ci-jenkins.jf.intel.com"
    url = "http://" + server + "/computer/api/python"
    host_dict = ast.literal_eval(reliable_url_open(url, method="POST").read().decode('utf-8'))
    for a_host in host_dict['computer']:
        offline = a_host['offline']
        if offline:
            print("Skipping system because it is offline: "
                  + a_host['displayName'])
            continue
        host = a_host['displayName']
        if is_excluded(host):
            continue
        options = { 'label' : host}
        url = f"http://{server}/job/public/job/delete_workspaces/buildWithParameters?{urlencode(options)}"
        print("triggering " + url)
        reliable_url_open(url, method="POST")
        time.sleep(1)

if os.path.exists("/home/jenkins/workspace"):
    remove_workspace_files()
    sys.exit(0)

# else
trigger_builds()
